//funcion validar campos con JQUERY
$(document).ready(function()
{
    $("#packages").validate(
        {
            rules:{
                name:{
                    required: true,
                    minlength: 3      
                },
                email:{
                    required: true,
                    email: true
                }
            },
            messages:{
                name:{
                    minlength: "Name should be at least 3 characters"
                },
                email:{
                    email: "It must be a valid email"
                }
            }
        });
});

//Validar que este seleccionado un paquete
$(document).ready(function(){

    $("#packages").submit(function(e)
    {
       if( $('#cbox1').prop("checked")== false && $('#cbox3').prop("checked")==false && $('#cbox4').prop("checked")==false 
            && $('#cbox8').prop("checked")==false && $('#cbox11').prop("checked")==false){
        alert("You must select a package");
        e.preventDefault();
      }
    });

    

//habilitar otros checkbox 
//Desmarcar chechbox
    $("#cbox1").on('change', function () 
    {   
        if($(this).is(':checked'))
        { 
        $('#cbox2').prop("disabled", false);
     }else{
        $('#cbox2').prop("disabled", true);
        $('#cbox2').prop("checked", false);
     }
    });

    $("#cbox4").on('change', function () 
    {   
        if($(this).is(':checked'))
        { 
        $('#cbox5').prop("disabled", false);
        $('#cbox6').prop("disabled", false);
        $('#cbox7').prop("disabled", false);
     }else{
        $('#cbox5').prop("disabled", true);
        $('#cbox6').prop("disabled", true);
        $('#cbox7').prop("disabled", true);

        $('#cbox5').prop("checked", false);
        $('#cbox6').prop("checked", false);
        $('#cbox7').prop("checked", false);
     }
    });


    $("#cbox8").on('change', function () 
    {   
        if($(this).is(':checked'))
        { 
        $('#cbox9').prop("disabled", false);
        $('#cbox10').prop("disabled", false);
     }else{
        $('#cbox9').prop("disabled", true);
        $('#cbox10').prop("disabled", true);
        $('#cbox9').prop("checked", false);
        $('#cbox10').prop("checked", false);
     }
    });


    $("#cbox11").on('change', function () 
    {   
        if($(this).is(':checked'))
        { 
        $('#cbox12').prop("disabled", false);
        $('#cbox13').prop("disabled", false);
        $('#cbox14').prop("disabled", false);
        $('#cbox15').prop("disabled", false);
        $('#cbox16').prop("disabled", false);
        $('#cbox17').prop("disabled", false);
        $('#cbox18').prop("disabled", false);
        $('#cbox19').prop("disabled", false);
     }else{

         $('#cbox12').prop("disabled", true);
         $('#cbox13').prop("disabled", true);
         $('#cbox14').prop("disabled", true);
         $('#cbox15').prop("disabled", true);
         $('#cbox16').prop("disabled", true);
         $('#cbox17').prop("disabled", true);
         $('#cbox18').prop("disabled", true);
         $('#cbox19').prop("disabled", true);

         $('#cbox12').prop("checked", false);
         $('#cbox13').prop("checked", false);
         $('#cbox14').prop("checked", false);
         $('#cbox15').prop("checked", false);
         $('#cbox16').prop("checked", false);
         $('#cbox17').prop("checked", false);
         $('#cbox18').prop("checked", false);
         $('#cbox19').prop("checked", false);
     }
    });
});

